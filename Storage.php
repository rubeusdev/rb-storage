<?php

namespace Rubeus\Storage;

use Google\Cloud\Storage\StorageClient;

class Storage
{
        
    public function __construct()
    {
        $this->execShell();
    }

    private function execShell()
    {
        shell_exec('export GOOGLE_APPLICATION_CREDENTIALS="' . GOOGLE_APPLICATION_CREDENTIALS . '"');
    }

    private static function getStorage()
    {
        $storage = new StorageClient([
            'projectId' => PROJECT_ID,
        ]);
        $bucket = $storage->bucket('rb-arquivos-teste');

        return $bucket;
    }

    private static function uploadObject($objectName, $source)
    {
        $file = fopen($source, 'r');
        $bucket = getStorage();
        $object = $bucket->upload($file, [
            'name' => $objectName,
            'predefinedAcl' => 'publicRead',
        ]);

        return $object;
    }

    public static function deleteObject($objectName)
    {
        $bucket = getStorage();
        $object = $bucket->object($objectName);
        $object->delete();
 
        return $object;
    }

    public static function fileOpen($patch)
    {
        if (STORAGE_ON) {
            return true;
        }
        return true;
    }

    public static function fileWrite($patch)
    {
        if (STORAGE_ON) {
            return true;
        }
        return true;
    }

    public static function fileRead($patch)
    {
        if (STORAGE_ON) {
            return true;
        }
        return true;
    }

    public static function fileClose($patch)
    {
        if (STORAGE_ON) {
            return true;
        }
        return true;
    }

    public static function fileDelete($patch)
    {
        if (STORAGE_ON) {
            return true;
        }
        return true;
    }

}
