<?php

namespace Rubeus\CloudStorage;

class CloudStorage
{
    private static $requisicao;
    private static $intervalo = BUCKET;

    private static function autorizarApiCloudStorage($scope = 'https://www.googleapis.com/auth/devstorage.full_control')
    {
        putenv('GOOGLE_APPLICATION_CREDENTIALS=' . GOOGLE_APPLICATION_CREDENTIALS);
        $cliente = new \Google_Client();
        $cliente->useApplicationDefaultCredentials();
        $cliente->addScope($scope);
        self::$requisicao = new \Google_Service_Storage($cliente);
    }

    public static function listaObjetosStorage($caminhoStorage)
    {
        self::autorizarApiCloudStorage();

        try {
            $objetos = self::$requisicao->objects->listObjects(
                self::$intervalo,
                [
                    'prefix' => $caminhoStorage
                ]
            );
            return $objetos->getItems();
        } catch (\Exception $e) {
            return false;
        }
    }

    public function verificaArquivo($objeto, $caminhoPastaStorage)
    {
        $pastaStorageExplodida = explode('/', $caminhoPastaStorage);
        $tamanhoPastaStorage = count($pastaStorageExplodida);
        if (empty(end($pastaStorageExplodida))) {
            $tamanhoPastaStorage--;
        }
        $caminhoObjeto = explode('/', $objeto->name);
        $tamanhoCaminhoObjeto = count($caminhoObjeto);
        if ($tamanhoCaminhoObjeto == ($tamanhoPastaStorage + 1)) {
            return true;
        }
        return false;
    }

    public static function retornaObjeto($caminhoObjetoStorage)
    {
        self::autorizarApiCloudStorage();

        try {
            $objetoBucket = self::$requisicao->objects->get(
                self::$intervalo,
                $caminhoObjetoStorage,
                ['alt' => 'media']
            )->getBody();
        } catch (\Exception $e) {
            $objetoBucket = null;
        }

        return $objetoBucket;
    }

    public static function insereObjetoStorage($objeto, $caminhoObjetoStorage, $tipoMime, $tipoPermissao = 'publicRead')
    {
        $tentativa = 0;
        self::autorizarApiCloudStorage();

        $postBody = self::criarPostBody($objeto, $caminhoObjetoStorage, $tipoMime, $tipoPermissao);

        do {
            $gsso = new \Google_Service_Storage_StorageObject;
            $gsso->setName($caminhoObjetoStorage);
            $gsso->setContentType($tipoMime);
            $gsso->setCacheControl('no-cache');

            try {
                self::uploadObjeto($gsso, $postBody);
            } catch (\Exception | \GoogleException $e) {
                $tentativa++;
                sleep(1);
                if ($tentativa == 2) {
                    sleep(5);
                    self::autorizarApiCloudStorage();
                    $postBody = self::criarPostBody($objeto, $caminhoObjetoStorage, $tipoMime, $tipoPermissao);
                }
                continue;
            }

            break;
        } while ($tentativa < 5);
    }

    private static function criarPostBody($objeto, $caminhoObjetoStorage, $mime, $tipoPermissao = 'publicRead')
    {
        $postBody = [
            'data' => $objeto,
            'name' => $caminhoObjetoStorage,
            'mimeType' => $mime,
            'uploadType' => 'multipart',
            'predefinedAcl' => $tipoPermissao,
        ];

        return $postBody;
    }

    private static function uploadObjeto($gsso, $postBody)
    {
        self::$requisicao->objects->insert(
            self::$intervalo,
            $gsso,
            $postBody
        );
    }

    public static function verificaPermissao()
    {
        if (defined('STORAGE_ON')) {
            return STORAGE_ON;
        }
        return false;
    }
}
