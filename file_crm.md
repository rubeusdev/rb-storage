# CRM
## file_get_contents

* crm/api/ExportarAgendamentos.php
* crm/api/ExportarOportunidades.php
* crm/api/ExportarPessoas.php
* crm/api/wsdl.php
* crm/bin/Config/registrarSSLRetroativos.php
* crm/bin/Config/scriptCriarConfig.php
* crm/bin/Config/scriptMoveFilesNg2Canal.php
* crm/bin/Config/scriptReplaceVariavel.php
* crm/bin/Db/atualizarBases.php
* crm/mosaico-php/editor.php
* crm/src/Comercial/Aplicacao/Agendamento/ExportarAgendamentos.php
* crm/src/Comercial/Aplicacao/Oportunidade/ExportarOportunidades.php
* crm/src/Comercial/Aplicacao/Pessoa/ExportarPessoas.php
* crm/src/Comercial/Aplicacao/Pessoa/ImportarCsv.php
* crm/src/Comercial/Aplicacao/Pessoa/SubstituirVariaveisModelo.php
* crm/src/Comercial/Aplicacao/RDStation/CadastroPessoa.php
* crm/src/Comercial/Aplicacao/AtividadeAutomatica/DadosAcao.php
* crm/src/Comercial/Aplicacao/Usuario/EnviarEmailDifinirSenha.php
* crm/src/Geral/Aplicacao/ElasticEmail/ServicoEmail.php
* crm/src/Geral/Aplicacao/Email/AlterarEnderecosEmail.php
* crm/src/Geral/Aplicacao/Email/BuscarDadosModelos.php
* crm/src/Geral/Aplicacao/Email/DadosEmail.php
* crm/src/Geral/Aplicacao/Email/EnviarEmail.php
* crm/src/Geral/Aplicacao/Integracao/LogProcesso.php
* crm/src/Geral/Aplicacao/Servico/EnviarSms.php
* crm/src/Geral/Aplicacao/Sms/BackgroundEnviarSmsMobiPronto.php
* crm/src/Geral/Aplicacao/Usuario/EnviarEmailRecuperarSenha.php

## file_put_contents

* crm/bin/Config/scriptCriarConfig.php
* crm/bin/Config/scriptMoveFilesNg2Canal.php
* crm/bin/Config/scriptReplaceVariavel.php
* crm/src/Comercial/Aplicacao/Agendamento/ExportarAgendamentos.php
* crm/src/Comercial/Aplicacao/Cta/CadastroVariacao.php
* crm/src/Comercial/Aplicacao/Oportunidade/ExportarOportunidades.php
* crm/src/Comercial/Aplicacao/Pessoa/ExportarPessoas.php
* crm/src/Geral/Aplicacao/ElasticEmail/ElasticEmail.php
* crm/src/Geral/Aplicacao/Email/AlterarEnderecosEmails.php
* crm/src/Geral/Aplicacao/Email/DownloadEmail.php
* crm/src/Geral/Aplicacao/Email/DuplicarEmail.php
* crm/src/Geral/Aplicacao/Email/EnviarEmail.php
* crm/src/Geral/Aplicacao/Email/SalvarDadosModelo.php
* crm/src/Geral/Aplicacao/Email/SalvarEmail.php

## fopen

* crm/bin/Config/registrarSSLRetroativos.php
* crm/src/Comercial/Aplicacao/Agendamento/ExportarAgendamentos.php
* crm/src/Comercial/Aplicacao/Oportunidade/ExportarOportunidades.php
* crm/src/Comercial/Aplicacao/Pessoa/ExportarPessoas.php
* crm/src/Comercial/Aplicacao/Pessoa/GerarModeloCsv.php
* crm/src/Comercial/Aplicacao/Pessoa/ImportarCsv.php
* crm/src/Comercial/Aplicacao/ElasticEmail/GerenciarCsv.php

## move_uploaded_file

* crm/src/Geral/Aplicacao/Email/UploadEmail.php


# Vendor/rubeus

## file_get_contents

* front-controller/Pagina.php
* integration-totvs/IniciarBase/Base.php
* integration-totvs/IniciarBase/LerBaseXml.php
* integration-totvs/ProcessoIntegracao/PrepararEnvio/PrepararXMLEnvioProcesso.php
* integration-totvs/WebService/ClienteTotvsWSLocal.php
* integration-totvs/WebService/LocalTotvsWS.php
* navigation/ControleNavegacao.php
* navigation/Dependencia.php
* newsletter/Aplicacao/Email.php
* services/Email/PHPMailer.class.php
* services/Emcrypt/Emcrypt.php
* services/Entrada/I.php
* services/Json/Json.php
* services/Pdf/TFPDF.php

## file_put_contents

* front-controller/Pagina.php
* generate-project/Base/GerarBase.php
* generate-project/Base/GerarBase_2.php
* generate-project/Base/GerarBase_3.php
* integration-totvs/IniciarBase/Base.php
* integration-totvs/IniciarBase/PuxarDados.php
* integration-totvs/WebService/ClienteTotvsWSLocal.php
* integration-totvs/WebService/LocalTotvsWS.php
* services/Email/PHPMailer.class.php
* services/Json/Json.php

## fopen

* integration-totvs/lib/class.soap_server.php
* integration-totvs/lib/class.wsdl.php
* integration-totvs/lib/class.wsdlcache.php
* integration-totvs/lib/nusoap.php
* integration-totvs/lib/nusoapmime.php
* services/Base64/Upload.php
* services/Email/PHPMailer.php
* services/Pdf/TFPDF.php
* services/Pdf/font/unifont/ttfonts.php

## move_uploaded_file

* services/Imagem/Imagem.php
* services/Upload/Upload.php

## convert -resize
pode ser substituido pela extenção do PHP

* services/Imagem/Imagem.php

# Total

65 Arquivos.

crm/api/ExportarAgendamentos.php
crm/api/ExportarOportunidades.php
crm/api/ExportarPessoas.php
crm/api/wsdl.php
crm/bin/Config/registrarSSLRetroativos.php
crm/bin/Config/scriptCriarConfig.php
crm/bin/Config/scriptMoveFilesNg2Canal.php
crm/bin/Config/scriptReplaceVariavel.php
crm/bin/Db/atualizarBases.php
crm/mosaico-php/editor.php
crm/src/Comercial/Aplicacao/Agendamento/ExportarAgendamentos.php
crm/src/Comercial/Aplicacao/Oportunidade/ExportarOportunidades.php
crm/src/Comercial/Aplicacao/Pessoa/ExportarPessoas.php
crm/src/Comercial/Aplicacao/Pessoa/ImportarCsv.php
crm/src/Comercial/Aplicacao/Pessoa/SubstituirVariaveisModelo.php
crm/src/Comercial/Aplicacao/RDStation/CadastroPessoa.php
crm/src/Comercial/Aplicacao/AtividadeAutomatica/DadosAcao.php
crm/src/Comercial/Aplicacao/Usuario/EnviarEmailDifinirSenha.php
crm/src/Geral/Aplicacao/ElasticEmail/ServicoEmail.php
crm/src/Geral/Aplicacao/Email/AlterarEnderecosEmail.php
crm/src/Geral/Aplicacao/Email/BuscarDadosModelos.php
crm/src/Geral/Aplicacao/Email/DadosEmail.php
crm/src/Geral/Aplicacao/Email/EnviarEmail.php
crm/src/Geral/Aplicacao/Integracao/LogProcesso.php
crm/src/Geral/Aplicacao/Servico/EnviarSms.php
crm/src/Geral/Aplicacao/Sms/BackgroundEnviarSmsMobiPronto.php
crm/src/Geral/Aplicacao/Usuario/EnviarEmailRecuperarSenha.php
crm/src/Comercial/Aplicacao/Cta/CadastroVariacao.php
crm/src/Geral/Aplicacao/ElasticEmail/ElasticEmail.php
crm/src/Geral/Aplicacao/Email/AlterarEnderecosEmails.php
crm/src/Geral/Aplicacao/Email/DownloadEmail.php
crm/src/Geral/Aplicacao/Email/DuplicarEmail.php
crm/src/Geral/Aplicacao/Email/SalvarDadosModelo.php
crm/src/Geral/Aplicacao/Email/SalvarEmail.php
crm/src/Comercial/Aplicacao/Pessoa/GerarModeloCsv.php
crm/src/Comercial/Aplicacao/ElasticEmail/GerenciarCsv.php
crm/src/Geral/Aplicacao/Email/UploadEmail.php
crm/vendor/rubeus/front-controller/Pagina.php
crm/vendor/rubeus/integration-totvs/IniciarBase/Base.php
crm/vendor/rubeus/integration-totvs/IniciarBase/LerBaseXml.php
crm/vendor/rubeus/integration-totvs/ProcessoIntegracao/PrepararEnvio/PrepararXMLEnvioProcesso.php
crm/vendor/rubeus/integration-totvs/WebService/ClienteTotvsWSLocal.php
crm/vendor/rubeus/integration-totvs/WebService/LocalTotvsWS.php
crm/vendor/rubeus/navigation/ControleNavegacao.php
crm/vendor/rubeus/navigation/Dependencia.php
crm/vendor/rubeus/newsletter/Aplicacao/Email.php
crm/vendor/rubeus/services/Email/PHPMailer.class.php
crm/vendor/rubeus/services/Emcrypt/Emcrypt.php
crm/vendor/rubeus/services/Entrada/I.php
crm/vendor/rubeus/services/Json/Json.php
crm/vendor/rubeus/services/Pdf/TFPDF.php
crm/vendor/rubeus/generate-project/Base/GerarBase.php
crm/vendor/rubeus/generate-project/Base/GerarBase_2.php
crm/vendor/rubeus/generate-project/Base/GerarBase_3.php
crm/vendor/rubeus/integration-totvs/IniciarBase/PuxarDados.php
crm/vendor/rubeus/integration-totvs/lib/class.soap_server.php
crm/vendor/rubeus/integration-totvs/lib/class.wsdl.php
crm/vendor/rubeus/integration-totvs/lib/class.wsdlcache.php
crm/vendor/rubeus/integration-totvs/lib/nusoap.php
crm/vendor/rubeus/integration-totvs/lib/nusoapmime.php
crm/vendor/rubeus/services/Pdf/font/unifont/ttfonts.php
crm/vendor/rubeus/services/Base64/Upload.php
crm/vendor/rubeus/services/Email/PHPMailer.php
crm/vendor/rubeus/services/Imagem/Imagem.php
crm/vendor/rubeus/services/Upload/Upload.php

# Clonar

git clone git@bitbucket.org:rubeusdev/rb-front-controller.git  
git clone git@bitbucket.org:rubeusdev/rb-integration-totvs.git  
git clone git@bitbucket.org:rubeusdev/rb-navigation.git  
git clone git@bitbucket.org:rubeusdev/rb-newsletter.git  
git clone git@bitbucket.org:rubeusdev/rb-services.git  
git clone git@bitbucket.org:rubeusdev/rb-generate-project.git


# Caminhos para se fazer

* Criar dependencia que abstrai as funcionalidades do G.C. Storage
    * a funcionalidade de verificar se o crm usará ou não o storage deve pertencer à esta classe?
    * deverá utilizar métodos estáticos ou classe instanciável?

<!-- * Utilizar classe padrão só com algumas funções básicas do G.C. Storage e nela teria toda a regra necessária para o funcionamento geral **NO CRM** as dependências seriam com funções nativas da biblioteca padrão da ferramenta -->

* funções de deletar arquivo

* funções do fopen, fread, fclose, fwrite

* usar a dependencia pelo conteiner do zé

* fazer o upload dos arquivos atuais e garantir o funcionamento dos links antigos


# Sessão Redis

* criar máquina com redis, utilizada para a sessão

# Fila

* arquivos temporários não precisam ir pro storage, mas tem que ir pra uma fila e retornar o q deve
    * Inteligencia da fila de saber se vai ou não executar o que está na fila, olhando o    processamento da máquina (checkar api da gcloud ou proxysql)

* verificar exec, shell_exec e a outra q o zé vai falar para garantir de não matar a máquina e o processo estar executando e posteriormente jogar ele para uma fila
    * tirar todas as filas atuais no crm, para criar uma nova fila simples ou jogar pra api queuer
    
* Smart queue

* Importação, Exportação, Envio segundo plano
    * crm -> queue -> crm local -> email

# Dependencias

* pacote para o envio da requisição da fila

# Contrapontos

* arquivos para edição, terá que ser salvo localmente?
